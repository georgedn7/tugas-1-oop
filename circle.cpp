#include <iostream>
using std::cout;

class Circle {
public:
    float radius;
    float getRadius(){
        return radius;
    }
    void setRadius(float r){
        radius=r;
    }
    float getArea(){
        return radius*radius*3.14;
    }
    float getCircumference(){
        return 2*3.14*radius;
    }
    Circle(float r) {
        radius=r;
    }
};

int main ()
{
    Circle lingkaran1= Circle(5.5);
    cout<<"Radius: "<<lingkaran1.getRadius()<<std::endl;
    cout<<"Area: "<<lingkaran1.getArea()<<std::endl;
    cout<<"Circumference: "<<lingkaran1.getCircumference()<<std::endl;

    Circle lingkaran2= Circle(11);
    cout<<"Radius: "<<lingkaran2.getRadius()<<std::endl;
    cout<<"Area: "<<lingkaran2.getArea()<<std::endl;
    cout<<"Circumference: "<<lingkaran2.getCircumference()<<std::endl;

    Circle lingkaran3= Circle(14.7);
    cout<<"Radius: "<<lingkaran3.getRadius()<<std::endl;
    cout<<"Area: "<<lingkaran3.getArea()<<std::endl;
    cout<<"Circumference: "<<lingkaran3.getCircumference()<<std::endl;
}